import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from "../user/user.entity";
import { Post } from "../post/post.entity";

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  content: string;

  @Column("timestamp", {default: () => "CURRENT_TIMESTAMP"})
  update_at: string;

  @Column("timestamp", {default: () => "CURRENT_TIMESTAMP"})
  created_at: string;

  @ManyToOne(() => User, (user) => user.comments)
  user: User

  @ManyToOne(() => Post, (post) => post.comments)
  post: Post
}