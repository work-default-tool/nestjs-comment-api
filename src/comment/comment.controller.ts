import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
} from '@nestjs/common';

import { GetUser } from '../auth/decorator';
import { JwtGuard } from '../auth/guard';
import { CommentService } from './comment.service';
import { CreateCommentDto } from './dto/create-comment.dto';

@Controller('comment')
export class CommentController {
  constructor(
    private commentService: CommentService,
  ) {}

  @Get()
  getComments() {
    return this.commentService.getComments();
  }

  @UseGuards(JwtGuard)
  @Post()
  createComment(
    @GetUser() user,
    @Body() dto: CreateCommentDto,
  ) {
    return this.commentService.createComment(user, dto);
  }
}
