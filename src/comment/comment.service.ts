import { Injectable, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Comment } from './comment.entity';
import { Post } from '../post/post.entity';
import { CreateCommentDto } from './dto/create-comment.dto';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private commentRepository: Repository<Comment>,
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
  ) {}

  async getComments(): Promise<Comment[]> {
    return await this.commentRepository.find();
  }

  async createComment(user, dto: CreateCommentDto): Promise<Comment> {
    const post = await this.postRepository.findOne({
      where: {
        id: dto.postId
      }
    });

    if (!post) 
      throw new ForbiddenException(
        'Post incorrect',
      );

    const comment = this.commentRepository.create({
      user: user,
      post: post,
      content: dto.content,
    })
    return await this.commentRepository.save(comment);
  }
}
