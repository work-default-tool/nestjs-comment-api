import {
  IsNotEmpty,
  IsString,
} from 'class-validator';

export class UpdateMessageDto {
  @IsString()
  name?: string;

  @IsString()
  content?: string;
}
