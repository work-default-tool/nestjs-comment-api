import {
  Body,
  Controller,
  Param,
  Get,
  Post,
  Put,
  Delete,
} from '@nestjs/common';

import { MessageService } from './message.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';

@Controller('message')
export class MessageController {
  constructor(
    private messageService: MessageService,
  ) {}

  @Get()
  getMessages() {
    return this.messageService.getMessages();
  }

  @Get(':id')
  findMessage(@Param('id') id: string) {
    return this.messageService.findMessage(id);
  }

  @Post()
  createMessage(
    @Body() createMessageDto: CreateMessageDto,
  ) {
    return this.messageService.createMessage(createMessageDto);
  }

  @Put(':id')
  updateMessage(@Param('id') id: string, @Body() updateMessageDto: UpdateMessageDto) {
    return this.messageService.updateMessage(id, updateMessageDto);
  }

  @Delete(':id')
  deleteMessage(@Param('id') id: string) {
    return this.messageService.deleteMessage(id);
  }
}
