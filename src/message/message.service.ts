import { Injectable, ForbiddenException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Message } from './message.schema';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';

@Injectable()
export class MessageService {
  constructor(
    @InjectModel(Message.name) 
    private messageModel: Model<Message>,
  ) {}

  async getMessages(): Promise<Message[]> {
    return await this.messageModel.find().exec();
  }

  async findMessage(id: string): Promise<Message> {
    return await this.messageModel.findById(id).exec();
  }

  async createMessage(createMessageDto: CreateMessageDto): Promise<Message> {
    return await new this.messageModel({
      ...createMessageDto, 
      createdAt: new Date(),
    }).save();
  }

  async updateMessage(id: string, updateMessageDto: UpdateMessageDto): Promise<Message> {
    return await this.messageModel.findByIdAndUpdate(id, updateMessageDto).exec();
  }

  async deleteMessage(id: string) {
    return await this.messageModel.findByIdAndDelete(id).exec();
  }
}
