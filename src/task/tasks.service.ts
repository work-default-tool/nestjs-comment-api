import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Repository } from 'typeorm';
import { Comment } from '../comment/comment.entity';
import * as fs from 'fs/promises';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Comment)
    private commentRepository: Repository<Comment>,
  ) {}

  // @Cron( '*/60 * * * *' )
  // openForBusiness()  {
  //     console.log("Delicious cakes is open for business...")
  // }

  @Cron(CronExpression.EVERY_HOUR)
  async createCommentReport() {
    const commentCount = await this.commentRepository.count();

    const person = await this.commentRepository
      .createQueryBuilder('comment')
      .select(['comment.*'])
      .groupBy('comment.userId')
      .getRawMany();

    const personCount = Object.keys(person).length;
    const outputText =
      'count comment:' + commentCount + ',count person:' + personCount;

    await fs
      .writeFile('commentReport.txt', outputText, 'utf-8')
      .then((res) => {
        console.log('success');
      })
      .catch((err) => {
        console.error('fail', err);
      });

    // async function writeFileAsync() {
    //   try {
    //     await fs.writeFile('commentReport.txt', outputText, 'utf-8');
    //     console.log('success');
    //   } catch (error) {
    //     console.error('fail', error);
    //   }
    // }

    // writeFileAsync();
  }
}

