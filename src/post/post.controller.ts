import {
  Body,
  Controller,
  Get,
  Post,
  Param,
  UseGuards,
} from '@nestjs/common';

import { GetUser } from '../auth/decorator';
import { JwtGuard } from '../auth/guard';
import { PostService } from './post.service';
import { CreatePostDto } from './dto/create-post.dto';
import { CreatePostCommentDto } from './dto/create-post-comment.dto';

@Controller('post')
export class PostController {
  constructor(
    private postService: PostService,
  ) {}

  @Get()
  getPosts() {
    return this.postService.getPosts();
  }

  @Get(':id')
  getPost(@Param('id') id: string) {
    return this.postService.getPost(id);
  }

  @Get(':id/comments')
  getPostComments(@Param('id') id: string) {
    return this.postService.getPostComments(id);
  }

  @UseGuards(JwtGuard)
  @Post()
  createPost(
    @GetUser() user,
    @Body() dto: CreatePostDto,
  ) {
    return this.postService.createPost(user, dto);
  }

  @UseGuards(JwtGuard)
  @Post(':id/comment')
  createPostComment(
    @GetUser() user,
    @Param('id') id: string,
    @Body() dto: CreatePostCommentDto,
  ) {
    return this.postService.createPostComment(user, id, dto);
  }
}
