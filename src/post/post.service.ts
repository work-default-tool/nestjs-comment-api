import { Injectable, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Post } from './post.entity';
import { Comment } from '../comment/comment.entity';
import { CreatePostDto } from './dto/create-post.dto';
import { CreatePostCommentDto } from './dto/create-post-comment.dto';

@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
    @InjectRepository(Comment)
    private commentRepository: Repository<Comment>,
  ) {}

  async getPosts(): Promise<Post[]> {
    return await this.postRepository.find();
  }

  async getPost(postId): Promise<Post> {
    return await this.postRepository.findOne({
      where: {
          id: postId,
      },
    });
  }

  async getPostComments(postId): Promise<Post> {
    return await this.postRepository.findOne({
      where: {
          id: postId,
      },
      relations: {
        comments: true,
      },
    });
  }

  async createPost(user, dto: CreatePostDto): Promise<Post> {
    const post = this.postRepository.create({
      user: user,
      title: dto.title,
      content: dto.content,
    });

    return await this.postRepository.save(post);
  }

  async createPostComment(user, postId, dto: CreatePostCommentDto): Promise<Comment> {
    const post = await this.postRepository.findOne({
      where: {
        id: postId
      }
    });

    if (!post) 
      throw new ForbiddenException(
        'Post incorrect',
      );

    const comment = this.commentRepository.create({
      user: user,
      post: post,
      content: dto.content,
    })
    return await this.commentRepository.save(comment);
  }
}
