import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { User } from "../user/user.entity";
import { Comment } from "../comment/comment.entity";

@Entity()
export class Post {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  title: string;

  @Column('text')
  content: string;

  @Column("timestamp", {default: () => "CURRENT_TIMESTAMP"})
  update_at: string;

  @Column("timestamp", {default: () => "CURRENT_TIMESTAMP"})
  created_at: string;

  @ManyToOne(() => User, (user) => user.posts)
  user: User

  @OneToMany(() => Comment, (comment) => comment.post)
  comments: Comment[]
}