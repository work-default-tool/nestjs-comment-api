import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PostService } from './post.service';
import { Post } from './post.entity';
import { Comment } from '../comment/comment.entity';
import { CreatePostDto } from './dto/create-post.dto';
import { CreatePostCommentDto } from './dto/create-post-comment.dto';

describe('PostService', () => {
  let service: PostService;

  const mockPostRepository = {
    save: jest.fn(),
    find: jest.fn(),
    findOne: jest.fn(),
    create: jest.fn(),
  };

  const mockCommentRepository = {
    save: jest.fn(),
    find: jest.fn(),
    findOne: jest.fn(),
    create: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostService,
        {
          provide: getRepositoryToken(Post),
          useValue: mockPostRepository,
        },
        {
          provide: getRepositoryToken(Comment),
          useValue: mockCommentRepository,
        },
    ],
    }).compile();

    service = module.get<PostService>(PostService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('getPosts => should return an array of post', async () => {
    //arrange
    const post = {
      id: 1,
      title: 'aaa',
      content: 'abc',
    };

    const posts = [post];
    jest.spyOn(mockPostRepository, 'find').mockReturnValue(posts);

    //act
    const result = await service.getPosts();

    // assert
    expect(result).toEqual(posts);
    expect(mockPostRepository.find).toBeCalled();
  });

  it('getPost => should return an object of post', async () => {
    //arrange
    const post = {
      id: 1,
      title: 'aaa',
      content: 'abc',
    };

    jest.spyOn(mockPostRepository, 'findOne').mockReturnValue(post);

    //act
    const result = await service.getPost(1);

    // assert
    expect(result).toEqual(post);
    expect(mockPostRepository.find).toBeCalled();
  });

  it('getPostComments => should return an object of post and array of comment', async () => {
    //arrange
    const postComments = {
      id: 1,
      title: 'aaa',
      content: 'abc',
      comments: [
        {
          id: 1,
          content: 111,
          postId: 1
        }
        
      ]
    };

    jest.spyOn(mockPostRepository, 'findOne').mockReturnValue(postComments);

    //act
    const result = await service.getPostComments(1);

    // assert
    expect(result).toEqual(postComments);
    expect(mockPostRepository.find).toBeCalled();
  });

  it('createPost => Should create a new post and return its data', async () => {
    // arrange
    const createPostDto = {
      title: 'aaa',
      content: 'abc',
    } as CreatePostDto;

    const post = {
      id: 1,
      title: 'aaa',
      content: 'abc',
    } as Post;

    jest.spyOn(mockPostRepository, 'create').mockReturnValue(post);
    jest.spyOn(mockPostRepository, 'save').mockReturnValue(post);

    // act
    const result = await service.createPost(1, createPostDto);

    // assert
    expect(mockPostRepository.save).toBeCalled();
    expect(mockPostRepository.save).toBeCalledWith(post);

    expect(result).toEqual(post);
  });

  it('createPostComment => Should create a new post and return its data', async () => {
    // arrange
    const createPostCommentDto = {
      content: 'abc',
    } as CreatePostCommentDto;

    const post = {
      id: 1,
      title: 'aaa',
      content: 'abc',
    } as Post;

    const comment = {
      id: 1,
      content: 'abc',
      user: {
        id: 1,
        email: 'aaa@aaa.com',
        name: 'aaa',
      },
      post: {
        id: 1,
        title: 'title-1',
        content: 'post conetnt-1'
      },
    } as Comment;

    jest.spyOn(mockPostRepository, 'findOne').mockReturnValue(post);

    jest.spyOn(mockPostRepository, 'create').mockReturnValue(comment);
    jest.spyOn(mockPostRepository, 'save').mockReturnValue(comment);

    // act
    const result = await service.createPostComment(1, 1, createPostCommentDto);

    // assert
    expect(mockPostRepository.save).toBeCalled();
    expect(mockPostRepository.save).toBeCalledWith(comment);

    expect(result).toEqual(comment);
  });
});