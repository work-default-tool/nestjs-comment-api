import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { Post } from '../post/post.entity';
import { Comment } from '../comment/comment.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column("varchar", {
    length: 150,
    unique: true,
  })
  email: string;

  @Column("varchar", { length: 200, select: false })
  password: string;

  @Column("varchar", { length: 200, nullable:true })
  name: string;

  @Column("varchar", { nullable:true })
  refresh_token: string;

  // @Column("timestamp", {default: () => "CURRENT_TIMESTAMP"})
  // update_at: string;

  // @Column("timestamp", {default: () => "CURRENT_TIMESTAMP"})
  // created_at: string;

  @CreateDateColumn({ 
    type: 'timestamptz'
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamptz'
  })
  updated_at: Date;

  @OneToMany(() => Post, (post) => post.user)
  posts: Post[]

  @OneToMany(() => Comment, (comment) => comment.user)
  comments: Comment[]
}