import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MongooseModule } from '@nestjs/mongoose';

import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { CommentModule } from './comment/comment.module';
import { PostModule } from './post/post.module';
import { MessageModule } from './message/message.module';
import { TaskModule } from './task/task.module';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ChatGateway } from './chat/chat.gateway';

@Module({
  imports: [
    ConfigModule.forRoot({
      // envFilePath: ['.env.development.local', '.env.development'],
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 20008,
      username: 'user',
      password: '123456',
      database: 'nestjs-comment',
      entities: [
        'dist/**/*.entity.{ts,js}',
      ],
      synchronize: true, // never true in production!
    }),
    MongooseModule.forRoot('mongodb://root:root@127.0.0.1:27017'),
    ScheduleModule.forRoot(),
    PostModule,
    CommentModule,
    MessageModule,
    UserModule,
    AuthModule,
    TaskModule
  ],
  controllers: [AppController],
  providers: [AppService, ChatGateway],
})
export class AppModule {}
